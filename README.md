# Kosmologic

Kosmologic is a Boolean logic module in Kosmo format. Internally, it only uses NAND gates to perform all of the logical operations, which include AND, NAND, OR, NOR, XOR, XNOR, and NOT. It is heavily inspired by [Instruo's eãs module](https://www.instruomodular.com/product/eas/), including the normalled inputs which allow for a single input to affect all 'downstream' outputs.

Normalled connections:  
* In A → In B
* NAND Out → In C
* NOR Out → In F
* XOR Out → In G

This is a surprisingly versatile module. Simple clocks and gates can create more complex patterns and rhythms. Input an audio signal to create a gated fuzz effect and mix it in with the original dry audio to add some gentle fuzz or crunchy overdrive. Stereo signals can even be used by splitting the left and right channels into two different logical inputs (In C and In E, for example).

This module also served as an opportunity to experiment with using LED backlighting on sections the front panel without solder mask, and I'm quite pleased with the result (it looks better in person than it does in the picture below).

## Version

The current version of this module is 1.1. Version 1.0 had 100k series resistors on the inputs before the 100k pulldown resistors, effectively creating an unnecessary 50% voltage divider. These resistors have been removed.

There also used to be a trimpot that set the reference voltage for the comparator thresholds, with the intent of experimenting with audio inputs to see what created the best fuzz effect on the outputs. For bipolar audio signals, turns out you don't need that high of a threshold, so I replaced it with a basic 100k/1k voltage divider (R36 & R37). This trimpot is still shown on the images below.

I have not built this version of the module yet, but I see no reason why it shouldn't work as intended. Build at your own risk.

## Specs

Width: 5 cm  
Depth: ~5 cm  
Power: ±12V, 10-pin  
Current: TBD

## Gerbers

The gerber files were generated with the intent of being fabricated by JLCPCB. As such, they were created with JLCPCB's recommendations in mind, notably using the Protel filename extensions and the inclusion of "JLCJLCJLCJLC" on all boards. Other fabricators may have different recommendations/requirements.

## Photos

Front:  
![](images/kosmologic_front.jpg)

Back:  
![](images/kosmologic_back.jpg)

Side:  
![](images/kosmologic_side.jpg)

## Documentation

* [Schematic](schematic/kosmologic_schematic.pdf)
* [BOM](bom/Kosmologic v1.1_bom.md)
* [Interactive BOM](bom/Kosmologic v1.1 Interactive BOM.html)

## GitLab repository

* [https://gitlab.com/illucynic/kosmologic](https://gitlab.com/illucynic/kosmologic)