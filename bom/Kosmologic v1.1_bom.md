# Kosmologic.kicad_sch BOM

11/16/2023 9:23:09 PM

Generated from schematic by Eeschema 7.0.1

**Component Count:** 93

| Refs | Qty | Component | Description |
| ----- | --- | ---- | ----------- |
| C1, C2 | 2 | 10uF | Polarized capacitor, US symbol |
| C3–9 | 7 | 100nF | Unpolarized capacitor |
| D1, D2 | 2 | 1N5817 | 20V 1A Schottky Barrier Rectifier Diode, DO-41 |
| D3–9 | 7 | 1N4148 | 100V 0.15A standard switching diode, DO-35 |
| D10–16 | 7 | LED | Light emitting diode |
| J1 | 1 | Power_2x5 | Generic connector, double row, 02x05, odd/even pin numbering scheme (row 1 odd numbers, row 2 even numbers), script generated (kicad-library-utils/schlib/autogen/connector/) |
| J2 | 1 | AND Input A | Audio Jack, 2 Poles (Mono / TS) |
| J3 | 1 | AND Input B | Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) |
| J4 | 1 | OR Input A | Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) |
| J5 | 1 | OR Input B | Audio Jack, 2 Poles (Mono / TS) |
| J6 | 1 | XOR Input A | Audio Jack, 2 Poles (Mono / TS) |
| J7 | 1 | XOR Input B | Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) |
| J8 | 1 | NOT Input | Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) |
| J9 | 1 | AND Output | Audio Jack, 2 Poles (Mono / TS) |
| J10 | 1 | NAND Output | Audio Jack, 2 Poles (Mono / TS) |
| J11 | 1 | NOR Output | Audio Jack, 2 Poles (Mono / TS) |
| J12 | 1 | OR Output | Audio Jack, 2 Poles (Mono / TS) |
| J13 | 1 | XNOR Output | Audio Jack, 2 Poles (Mono / TS) |
| J14 | 1 | XOR Output | Audio Jack, 2 Poles (Mono / TS) |
| J15 | 1 | NOT Output | Audio Jack, 2 Poles (Mono / TS) |
| J16 | 1 | Conn_01x10_Female | Generic connector, single row, 01x10, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J17 | 1 | Conn_01x10_Male | Generic connector, single row, 01x10, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J18 | 1 | Conn_01x08_Female | Generic connector, single row, 01x08, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J19 | 1 | Conn_01x08_Male | Generic connector, single row, 01x08, script generated (kicad-library-utils/schlib/autogen/connector/) |
| Q1–7 | 7 | BC547 | 0.1A Ic, 45V Vce, Small Signal NPN Transistor, TO-92 |
| R1–14, R36 | 15 | 100k | Resistor, US symbol |
| R15, R17, R18, R20, R21, R23, R24, R26, R27, R29, R30, R32, R33, R35, R37 | 15 | 1k | Resistor, US symbol |
| R16, R19, R22, R25, R28, R31, R34 | 7 | 1.2k | Resistor, US symbol |
| U1–3 | 3 | 4011 | Quad Nand 2 inputs |
| U4, U5 | 2 | TL074 | Quad Low-Noise JFET-Input Operational Amplifiers, DIP-14/SOIC-14 |

